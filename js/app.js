// Patch

get = Ember.get,
set = Ember.set;

Ember.computed.controller = function(controllerName, contentPath) {
  return Ember.computed(contentPath, function() {
    var fullName = 'controller:' + controllerName;
    var controller = this.container.lookup(fullName, {singleton: false});
    console.log("CC - this: ", this.get('model'));
    console.log("CC - controller", controller);
    console.log("CC - contentPath: ", contentPath);
    console.log("CC - get(this, contentPath): ", get(this, contentPath));

    set(controller, 'content', get(this, contentPath));
    set(controller, 'parentController', this);
    set(controller, 'target', this);
    return controller;
  });
};

/* DEMO */

App = Em.Application.create();
// Routes

App.IndexRoute = Em.Route.extend({
  model: function() {
    console.log("IndexRoute::model");
    return this.store.find('tour', 1);
  },
  setupController: function(controller, model){
    console.log(controller);
    console.log("Model", model);
    controller.set("model", model);
    console.log(controller.get("model", model));
  }
});
  
// Really this is the tour controller
App.IndexController = Em.ObjectController.extend({
  init: function(){
    console.log("Index init");
    return this._super();
  },
  challenges: Em.computed.controller('challenges', 'model.challenges')
});

App.ChallengesController = Em.ArrayController.extend({
  itemController: 'challenge',
  init: function(){
    console.log("Challenges init");
    console.log(this.get("model"));
    return this._super();
  }
});
  
App.ChallengeController = Em.ObjectController.extend({
  init: function(){
    console.log("Challenge init");
    console.log(this.get("model"));
    return this._super();
  },
  facts: Em.computed.controller('facts', 'content.facts')
});
  
App.FactsController = Em.ArrayController.extend({
  init: function(){
    console.log("Facts init");
    console.log(this.get("model"));
    return this._super();
  },
  firstChildConstructed: true,
  itemController: 'fact',
  notifyFinished: function(childController){
    if(this.firstChildConstructed){
      console.log("First child controller:", childController);
      this.firstChildConstructed = false;
      childController.isActive = true;
    }
  },
  actions: {
    swapActive: function(){
      var childControllers = this.get("_subControllers")
      childControllers.objectAt(1).set("isActive", true);
      childControllers.objectAt(0).set("isActive", false);
    }
  }
});
  
App.FactController = Em.ObjectController.extend({
  init: function(){
    console.log("Fact init");
    console.log(this.get("model"));
    this.get("parentController").notifyFinished(this);
    return this._super();
  },
  isActive: false
});

// ========= MODELS =============

App.Tour = DS.Model.extend({
  title: DS.attr('string'),
  challenges: DS.hasMany('challenge')
});

App.Challenge = DS.Model.extend({
  name: DS.attr('string'),
  tour: DS.belongsTo('tour'),
  facts: DS.hasMany('fact')
});

App.Fact = DS.Model.extend({
  factName: DS.attr("name"),
  challenge: DS.belongsTo("challenge")
});

App.ApplicationAdapter = DS.FixtureAdapter.extend();

App.Store = DS.Store.extend({
  init: function(){
    this._super();
    this.push(App.Tour, {
        id: 1,
        title: "My tour",
        challenges: [1, 2]
    });

    this.push(App.Challenge, {
      id: 1,
      name: "First challenge",
      facts: [1, 2],
      tour: 1
    });
    this.push(App.Challenge, {
      id: 2,
      name: "Second challenge",
      facts: [3, 4],
      tour: 1
    });

    this.push(App.Fact,
      { id: 1, factName: "Fact one", challenge: 1 }
    );
    this.push(App.Fact,
      { id: 2, factName: "Fact two", challenge: 1 }
    );
    this.push(App.Fact,
      { id: 3, factName: "Fact three", challenge: 2 }
    );
    this.push(App.Fact,
      { id: 4, factName: "Fact four", challenge: 2 }
    );
  }
})

// ==============================
